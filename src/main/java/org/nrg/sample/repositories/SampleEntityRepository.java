package org.nrg.sample.repositories;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.sample.entities.SampleEntity;
import org.springframework.stereotype.Repository;

@Repository
public class SampleEntityRepository extends AbstractHibernateDAO<SampleEntity> {
}
