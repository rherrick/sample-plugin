package org.nrg.sample.rest;

import io.swagger.annotations.Api;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.sample.entities.SampleEntity;
import org.nrg.sample.services.SampleService;
import org.nrg.xdat.rest.AbstractXnatRestApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Api(description = "XNAT Sample API")
@XapiRestController
@RequestMapping(value = "/sample")
public class SampleRestController extends AbstractXnatRestApi {
    @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public ResponseEntity<List<SampleEntity>> getEntities() {

        return new ResponseEntity<>(_service.getAll(), HttpStatus.OK);
    }

    @RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public ResponseEntity<Void> createEntity(@RequestBody final SampleEntity entity) {
        _service.create(entity);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public ResponseEntity<SampleEntity> getEntity(@PathVariable final String id) {
        return new ResponseEntity<>(_service.findBySampleId(id), HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public ResponseEntity<Void> updateEntity(@PathVariable final String id, @RequestBody final SampleEntity entity) {
        final SampleEntity existing = _service.findBySampleId(id);
        existing.setSampleId(entity.getSampleId());
        existing.setMessage(entity.getMessage());
        _service.update(existing);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEntity(@PathVariable final String id) {
        final SampleEntity existing = _service.findBySampleId(id);
        _service.delete(existing);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @Autowired
    private SampleService _service;
}
