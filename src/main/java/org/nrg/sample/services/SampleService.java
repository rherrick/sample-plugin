package org.nrg.sample.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.sample.entities.SampleEntity;

public interface SampleService extends BaseHibernateService<SampleEntity> {
    SampleEntity findBySampleId(final String sampleId);
}
