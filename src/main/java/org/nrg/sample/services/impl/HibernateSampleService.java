package org.nrg.sample.services.impl;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.sample.entities.SampleEntity;
import org.nrg.sample.repositories.SampleEntityRepository;
import org.nrg.sample.services.SampleService;
import org.springframework.stereotype.Service;

@Service
public class HibernateSampleService extends AbstractHibernateEntityService<SampleEntity, SampleEntityRepository> implements SampleService {
    @Override
    public SampleEntity findBySampleId(final String sampleId) {
        return getDao().findByUniqueProperty("sampleId", sampleId);
    }
}
