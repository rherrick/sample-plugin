package org.nrg.sample.plugin;

public @interface SampleAnnotation {
    int value() default 0;
    String name() default "";
}
