package org.nrg.sample.plugin;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "sampleModule", name = "Sample Module", entityPackages = "org.nrg.sample.entities")
@ComponentScan({"org.nrg.sample.services.impl", "org.nrg.sample.repositories", "org.nrg.sample.rest"})
public class SampleXnatPlugin {
    @Bean
    public String sampleModuleMessage() {
        return "Hello there from the sample plugin!";
    }
}
