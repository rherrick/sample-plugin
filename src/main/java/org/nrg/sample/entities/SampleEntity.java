package org.nrg.sample.entities;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "sampleId"))
public class SampleEntity extends AbstractHibernateEntity {
    public String getSampleId() {
        return _sampleId;
    }

    public void setSampleId(final String sampleId) {
        _sampleId = sampleId;
    }

    public String getMessage() {
        return _message;
    }

    public void setMessage(final String message) {
        _message = message;
    }

    private String _sampleId;
    private String _message;
}
